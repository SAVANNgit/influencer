<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ezecom</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style>
        .header {
            background-color: #55168b;
        }

        /*// Extra small devices (portrait phones, less than 576px)*/
        /*// No media query since this is the default in Bootstrap*/

        /*// Small devices (landscape phones, 576px and up)*/
        /*@media (min-width: 576px) { ... }*/

        /*// Medium devices (tablets, 768px and up)*/
        /*@media (min-width: 768px) { ... }*/

        /*// Large devices (desktops, 992px and up)*/
        /*@media (min-width: 992px) { ... }*/

        /*// Extra large devices (large desktops, 1200px and up)*/
        /*@media (min-width: 1200px) { ... }*/
        .container-fluid {
            background-image: url("Web.webp");
            font-family: CoreSansD55Bold;
            color: white;
            height: 1079px;
            /*max-height: 1079px;*/
        }

        #sign-up, .gift-set-side {
            color: #ac7edb;
            font-size: 22px;
        }

        h2 {
            width: 281px;
            height: 82px;
        }

        .use-col-md {
            padding-top: 10%;
        }

        .logo-col-md {
            padding-top: 50px;
            padding-bottom: 20px;
        }

        .gift-set-side {
            padding-top: 20%;
        }
    </style>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>
</head>
<body>


<div class="header">
    <div class="row">
        <div class="col-md"></div>
        <div class="col-md"></div>
        <div class="col-md logo-col-md">
            <img src="Group%2030.svg" alt="">
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-md use-col-md">
            <div class="float-md-right">

                <div class="influencer-name">
                    <h2>ទិញកញ្ចប់អ៉ិនធឺណិត​
                        ជាមួយ SAM KOSAL</h2>
                    <h2 id="sign-up">SIGN UP FOR INTERNET
                        WITH SAM KOSAL</h2>
                </div>
                <div class="contact-form">
                    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
                        <ul>
                            <div class="group input-group mb-3">

                                <img src="person.svg" alt="">
                                <input type="text" required name="name">
                                <span class="highlight"></span>
                                <label>ឈ្មោះពេញ / Full Name *</label>
                            </div>
                        </ul>

                        <ul>
                            <div class="group input-group mb-3">

                                <img src="mobile.svg" alt="">
                                <input type="text" required name="mobile">
                                <span class="highlight"></span>
                                <label>លេខទូរស័ព្ទ / Phone Number *</label>
                            </div>
                            <li>
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="exampleCheck1" value="telegram">
                                    <label class="form-check-label" for="exampleCheck1">ភ្ជាប់ជាមួយតេលេក្រាម / Connect
                                        with
                                        Telegram</label>
                                </div>
                            </li>
                        </ul>

                        <ul>
                            <div class="group input-group">
                                <img src="person_and_start.svg" alt="">
                                <input type="text" required>
                                <span class="highlight"></span>
                                <label>ប្រូម៉ូកូត / Promo Code *</label>
                            </div>
                        </ul>
                        <ul>
                            <div class="group input-group">
                                <img src="world.svg" alt="">
                                <input type="text">
                                <span class="highlight"></span>
                                <label>កញ្ចប់ចាប់អារម្មណ៍ / Interested Package</label>
                            </div>
                            <li>
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="exampleCheck2">
                                    <label class="form-check-label" for="exampleCheck2">កញ្ចប់គេហដ្ឋាន /
                                        Home
                                        Package</label>
                                </div>
                            </li>
                            <li>
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="exampleCheck3">
                                    <label class="form-check-label" for="exampleCheck3">កញ្ចប់អាជីវកម្ម /
                                        Business Package</label>
                                </div>
                            </li>
                        </ul>
                        <ul>
                            <button class="btn btn-block send-btn" type="submit">ចុះឈ្មោះ / SIGN UP​</button>
                        </ul>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md">
        </div>
        <div class="col-md gift-set-side">
            <h2>កញ្ចប់កាដូពិសេស
                Special Gift Set</h2>

            <img src="gift-set.webp" alt="">

        </div>
    </div>
</div>

<style>
    .group {
        position: relative;
        margin-bottom: 25px;
    }

    .group > input {
        font-size: 13px;
        height: 25px;
        padding: 10px 10px 10px 5px;
        display: block;
        width: 300px;
        border: none;
        outline: none;
        border-bottom: 1px solid #B1B1B1;
        color: #B1B1B1;
        background: rgba(0, 0, 0, 0);
        opacity: 0.5;
        transition: 0.2s ease;
    }

    input:focus {
        outline: none;
        opacity: 1;
    }

    label {
        color: white;
        font-weight: normal;
        position: absolute;
        pointer-events: none;
        left: 24px;
        top: 2px;
        opacity: 2;
        transition: 0.2s ease all;
        -moz-transition: 0.2s ease all;
        -webkit-transition: 0.2s ease all;
    }

    /* active state */
    .group > input:focus ~ label, .group > input:valid ~ label {
        top: -10px;
        font-size: 12px;
        color: #B1B1B1;
        opacity: 1;
    }

    .highlight {
        position: absolute;
        height: 60%;
        width: 100px;
        top: 25%;
        left: 0;
        pointer-events: none;
        opacity: 0.5;
    }

    /* active state */
    input:focus ~ .highlight {
        -webkit-animation: inputHighlighter 0.3s ease;
        -moz-animation: inputHighlighter 0.3s ease;
        animation: inputHighlighter 0.3s ease;
    }

    /* ANIMATIONS ================ */
    @-webkit-keyframes inputHighlighter {
        from {
            background: #B1B1B1;
        }
        to {
            width: 0;
            background: transparent;
        }
    }

    @-moz-keyframes inputHighlighter {
        from {
            background: #B1B1B1;
        }
        to {
            width: 0;
            background: transparent;
        }
    }

    @keyframes inputHighlighter {
        from {
            background: #B1B1B1;
        }
        to {
            width: 0;
            background: transparent;
        }
    }

    button.send-btn {
        /*float: right;*/
        color: #B1B1B1;
        /*transition: 0.2s ease;*/
        border-radius: 20px;
    }

    button.send-btn:hover {
        color: #6a1faf;
        cursor: pointer;
    }

    ul {
        padding-left: 0px;
        margin-bottom: 2rem;
    }

    ul > li {
        padding-left: 24px;
    }

    .input-group > img {
        padding-right: 10px;
    }

    .form-check > label {
        font-size: 11px;
    }

    ::marker {
        color: #3f186b;
    }
</style>

<?php

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $FULLNAME = $_POST["name"];
    $PHONE = $_POST["mobile"];
    $EMAIL = "savann.nara@ezecomcorp.com";//$_POST["email"];
    $COMMENT = "Testing";//$_POST["desciption"];
    $PRODUCT = "";//$_POST["product"];
    $PRODUCT_STATUS = "";//$_POST["status"];
}
function check_duplicate($entity_type, $type, $value)
{

    $entity_type = strtoupper($entity_type);
    $type = strtoupper($type);
    // $value = strtoupper($value);

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://eze24.ezecomcorp.com/rest/479/l8rmccf4x2bker2p/crm.duplicate.findbycomm",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_SSL_VERIFYPEER => FALSE,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "{\r\n \"entity_type\": \"$entity_type\",\r\n \"type\": \"$type\",\r\n\"values\": [\r\n \"$value\"\r\n]\r\n}",
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "Cookie: PHPSESSID=jLtIEVnzgheoCKV0C6EOTsjJPnlsmRTO; LIVECHAT_GUEST_HASH=f2bc7291b33314fce524c307bd233bf6; BITRIX_SM_SALE_UID=0"
        ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);


    curl_close($curl);

    if ($err) {
        return "cURL Error #:" . $err;
    }
    return $response;
}

?>
</body>
</html>